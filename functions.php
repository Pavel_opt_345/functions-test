<?php
/* ========== Adjust plugin Yith Woocommerce Wishlist */
add_filter( 'yith_wcwl_product_added_to_wishlist_message', 'replace_category_name', 10, 1 );
function replace_category_name($message) {
	$product_id = intval($_POST['add_to_wishlist']);

	$terms = get_the_terms($product_id, 'product_cat');
	foreach ($terms as $term) {
	    $product_cat_id = $term->term_id;
	    break;
	}

	return str_replace("%name_of_category%", get_cat_name($product_cat_id), $message);
}

/* ========== Adjust credit card form to change expiry field ========== */
$payment_form_args = [];
$frontend_script_path = get_stylesheet_directory_uri() . '/woocommerce/assets/js/frontend/';
$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
wp_dequeue_script('wc-credit-card-form');
wp_enqueue_script( 'wc-credit-card-form', $frontend_script_path . 'credit-card-form' . $suffix . '.js', array( 'jquery', 'jquery-payment' ) );

add_filter( 'woocommerce_credit_card_form_args', 'receive_args', 10, 1);
add_filter( 'woocommerce_credit_card_form_fields', 'adjust_credit_card_form', 10, 2);

function receive_args($args) {
	global $payment_form_args;

	$payment_form_args = $args;

	return $args;
}

function adjust_credit_card_form($fields, $id) {
	global $payment_form_args;

	$fields['card-expiry-field'] = '<p class="form-row form-row-first">
				<label for="' . esc_attr( $id ) . '-card-expiry">' . __( 'Utløpsdato (MM/ÅÅ)', 'woocommerce' ) . ' <span class="required">*</span></label>
				<input id="' . esc_attr( $id ) . '-card-expiry" class="input-text wc-credit-card-form-card-expiry" type="text" autocomplete="off" placeholder="' . esc_attr__( 'MM / ÅÅ', 'woocommerce' ) . '" name="' . ( $payment_form_args['fields_have_names'] ? $id . '-card-expiry' : '' ) . '" />
			</p>';
	return $fields;
}

/* ====== To display Wishlist in product block ======= */
if ( in_array( 'yith-woocommerce-wishlist-premium/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ){
    
    if( ! function_exists( 'arise_add_to_wishlist_in_shop' ) ){

        function arise_add_to_wishlist_in_shop(){
            echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
        }
    }
    add_action( 'woocommerce_after_shop_loop_furnit', 'arise_add_to_wishlist_in_shop', 8 );
}

/* ====== To display Grid List in product block ======= */
if ( in_array( 'woocommerce-grid-list-toggle/woocommerce-grid-list-toggle.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    
    class My_WC_List_Grid extends WC_List_Grid{

        public function setup_gridlist() {
            
                    remove_action( 'woocommerce_after_shop_loop_item', array( $this, 'gridlist_buttonwrap_open' ), 9);
                    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
                    remove_action( 'woocommerce_after_shop_loop_item', array( $this, 'gridlist_buttonwrap_close' ), 11);

                    add_action( 'wp_enqueue_scripts', array( $this, 'setup_scripts_styles' ), 20);
                    add_action( 'wp_enqueue_scripts', array( $this, 'setup_scripts_script' ), 20);
                    add_action( 'woocommerce_before_shop_loop', array( $this, 'gridlist_toggle_button' ), 30);

                    add_action( 'woocommerce_after_shop_loop_furnit', array( $this, 'gridlist_buttonwrap_open'), 9);
                    add_action( 'woocommerce_after_shop_loop_furnit', 'addto_cart_hover_open');
                    add_action( 'woocommerce_after_shop_loop_furnit', 'woocommerce_template_loop_add_to_cart', 10 );
                    add_action( 'woocommerce_after_shop_loop_furnit', 'addto_cart_hover_close');
                    add_action( 'woocommerce_after_shop_loop_furnit', array( $this, 'gridlist_buttonwrap_close'), 11);

                    add_action( 'woocommerce_after_shop_loop_item', array( $this, 'gridlist_hr' ), 30);
                    add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 5);
                    add_action( 'woocommerce_after_subcategory', array( $this, 'gridlist_cat_desc' ) );
            }

            protected function  addto_cart_hover_open() {
                ?>
                    <div class="addto-cart-hover">
                <?php
            }

            protected function  addto_cart_hover_close() {
                ?>
                    </div>
                <?php
            }
    }
    $My_WC_List_Grid = new My_WC_List_Grid();
    $My_WC_List_Grid->setup_gridlist();
}
/* ====== To display Quick View in product block ======= */
if ( in_array( 'yith-woocommerce-quick-view/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    
    $foo = YITH_WCQV_Frontend();

    remove_action( 'woocommerce_after_shop_loop_item', array( $foo, 'yith_add_quick_view_button' ), 15 );

    add_action( 'woocommerce_after_shop_loop_furnit', 'yith_wcqv_button_wrapper_open', 14);
    add_action( 'woocommerce_after_shop_loop_furnit', array( $foo, 'yith_add_quick_view_button' ), 15 );
    add_action( 'woocommerce_after_shop_loop_furnit', 'yith_wcqv_button_wrapper_close', 16);

    function  yith_wcqv_button_wrapper_open() {
        ?>
            <div class="yith-wcqv-button-wrapper">
        <?php
    }

    function  yith_wcqv_button_wrapper_close() {
        ?>
            </div>
        <?php
    }
    
    function woocommerce_show_product_images() { 
        $wc_get_template = function_exists ( 'wc_get_template' ) ? 'wc_get_template' : 'woocommerce_get_template';
        $wc_get_template ( 'single-product/product-image-zoom.php', array (), '', get_stylesheet_directory() . '/templates/' );
    }
}
/* ====== To display product category ====== */
add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_category_product', 3);
function  woocommerce_template_category_product() {
    global $product;
    global $post;
    $categories = get_the_terms( $post->ID, 'product_cat');
    foreach ($categories as $category) { 
        $out .= '<a href="'.get_category_link($category->term_id ).'">'.$category->name.'</a>, ';
    }
    $out=substr(rtrim($out), 0, -1);
    echo '<p class="product_cat">'.$out.'</p>';
    $out = '';
}